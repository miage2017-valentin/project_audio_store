export const environment = {
  production: true,
  urlAPI: 'http://front-end-audiostore:3001',
  perPageHome: 5,
  perPageShop: 10
};
