import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { PluginAudio } from '../plugin-audio';
import { UserService } from '../user.service';
import { PluginAudioService } from '../plugin-audio.service';

@Component({
  selector: 'app-card-plugin',
  templateUrl: './card-plugin.component.html',
  styleUrls: ['./card-plugin.component.css']
})
export class CardPluginComponent implements OnInit {
  @Input()
  public plugin: PluginAudio;

  @Output()
  private onClickCategory = new EventEmitter<string>();

  @Output()
  private onClickDelete = new EventEmitter<boolean>();

  constructor(public user: UserService, public pluginAudioService: PluginAudioService) { }

  ngOnInit() {
  }

  clickOnCategory(category){
    console.log("Card -- "+ category);
    this.onClickCategory.emit(category);
  }

  deletePlugin(plugin){
    this.pluginAudioService.deletePluginById(plugin._id).then((response) => {
      console.log(response.message);
      this.onClickDelete.emit(true);
    })
    .catch(()=>{
      this.onClickDelete.emit(false);
    })
  }
}
