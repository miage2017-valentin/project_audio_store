import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPluginComponent } from './card-plugin.component';

describe('CardPluginComponent', () => {
  let component: CardPluginComponent;
  let fixture: ComponentFixture<CardPluginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardPluginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPluginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
