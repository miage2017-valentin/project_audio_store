import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { PluginAudio } from '../plugin-audio';
import { PluginAudioService } from '../plugin-audio.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-form-plugin',
  templateUrl: './form-plugin.component.html',
  styleUrls: ['./form-plugin.component.css']
})
export class FormPluginComponent implements OnInit {
  public _plugin: PluginAudio;
  @Input()
  public btnLabelValidate: string;
  @Output()
  private submit = new EventEmitter<any>();

  @Input()
  set plugin(plugin: PluginAudio) {
    this._plugin = plugin;
    this.url = this._plugin.image;
  }

  public control: any = {};
  public selectedFile: File;
  public displayedColumns: String[];
  public url: string;
  public visible = true;
  public selectable = true;
  public removable = true;
  public addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];


  constructor(private pluginAudioService: PluginAudioService) {
    this.displayedColumns = ['name', 'default', 'min', 'max', 'actions'];
  }

  ngOnInit() {
    this._plugin = { controls: [], categories: [] } as PluginAudio;
    this.url = environment.urlAPI + "/public/images/image-empty.png";
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.selectedFile = event.target.files[0];
      let reader = new FileReader();

      reader.readAsDataURL(this.selectedFile); // read file as data url

      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
    }
  }

  onAdd(): void {
    this._plugin.controls.push(this.control);
    this._plugin.controls = [...this._plugin.controls];
    this.control = {}
  }

  addCategory(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if (value !== '') {
      this._plugin.categories.push(value);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(category): void {
    const index = this._plugin.categories.indexOf(category);

    if (index >= 0) {
      this._plugin.categories.splice(index, 1);
    }
  }

  validForm() {
    this.submit.emit({ plugin: this._plugin, file: this.selectedFile });
  }

  removeControl(control){
    const index = this._plugin.controls.indexOf(control);
    console.log(index);
    if(index >= 0){
      this._plugin.controls.splice(index, 1);
      this._plugin.controls = [...this._plugin.controls];
    }
  }
}
