import { TestBed } from '@angular/core/testing';

import { PluginAudioService } from './plugin-audio.service';

describe('PluginAudioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PluginAudioService = TestBed.get(PluginAudioService);
    expect(service).toBeTruthy();
  });
});
