import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {UserService} from '../user.service';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';


@Component({
  selector: 'app-sign-form',
  templateUrl: './sign-form.component.html',
  styleUrls: ['./sign-form.component.css']
})
export class SignUpComponent implements OnInit {

  public login: String;
  public password: String;
  public passwordConfirm: String;
  public name: String;
  public url: String;
  
  constructor(
    public dialogRef: MatDialogRef<SignUpComponent>, private user: UserService, public snackBar: MatSnackBar) {}

  ngOnInit(){
    
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  clickSign(){
    this.user.register(this.login, this.name, this.password, this.passwordConfirm, this.url)
    .then(()=>{
      this.dialogRef.close();
    })
    .catch((message)=>{
      let config = new MatSnackBarConfig();
      config.panelClass = 'center-snackbar';
      config.duration = 1000;
      this.snackBar.open(message, '', config);
    })
  }

}
