const express = require('express');
const PluginAudioController = require("./controllers/PluginAudioController")
const UserController = require("./controllers/UserController")
const router = express.Router();
const auth = require('./middlewares/authentification');
const multer  = require('multer')
const upload = multer({ dest: 'public/images/' });

module.exports = () => {
    let pluginAudioController = new PluginAudioController();
    let userController = new UserController();

    router.get('/plugin/:id', pluginAudioController.getDetailsPlugin);
    router.put('/plugin/:id', auth, upload.single('image'), pluginAudioController.update);
    router.delete('/plugin/:id', auth, pluginAudioController.delete);
    router.post('/plugin', auth, upload.single('image'), pluginAudioController.create);
    router.get('/plugins', pluginAudioController.getListPluginAudio);
    router.get('/myPlugins', auth,pluginAudioController.getPluginOfCurrentUser);
    router.get('/plugins/count', pluginAudioController.count);

    //User
    router.post('/login', userController.login);
    router.post('/register', userController.register);
    router.get('/checkToken', userController.checkToken);
    router.get('/profile', auth, userController.profile);

    return router;
};