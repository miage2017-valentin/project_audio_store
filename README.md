# Prérequis
 - NodeJS version 11
 - Docker (pour la base de donnée "MongoDB" préconfigurée)

# Installation

Pour commencer, veuillez cloner le projet sur votre machine grâce à la commande suivante:
```
git clone https://bitbucket.org/miage2017-valentin/project_audio_store.git
```

Lancez un terminal puis positionnez vous à la racine du projet.

Lancez dans un premier temps Docker sur votre machine.
Puis lancez la base de données "MongoDB" avec cette commande:
```
docker-compose up -d
```

Executez ensuite toutes les commandes suivantes dans cet ordre:
```
chmod +x init.sh
./init.sh
cd back_end
npm start
```
Cela vous permettra d'initialiser les deux projets (front et back) en téléchargeant les modules.
De plus, l'API sera lancée sur le port 3001.

Pour lancez la partie front, exécutez les commandes suivantes dans un autre terminal:
```
cd front_end
npm install -g @angular/cli
ng serve
```
L'interface utilisateur est alors accessible par l'url: http://localhost:4200


Vous pouvez utiliser un utilisateur déjà créé sur l'application en rentrant ces identifiants:

 - login : modTeam
 - password : mod
 
 Une version déjà hébergée est disponible à cette adresse: http://195.154.77.42:4200